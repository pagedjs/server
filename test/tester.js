const fetch = require("node-fetch");
const util = require('util');
const fs = require('fs');
const readFile = util.promisify(fs.readFile);

(async () => {
  const html = await readFile("index.html", "utf-8");

  let file = await fetch('http://localhost:3000/printer', {
      method: 'POST',
      body: JSON.stringify({
        options: {},
        // url: "http://localhost:9090/examples/assets/ruby.html",
        html: html
      }),
      headers: {
        'Content-Type': 'application/json',
      },
      // responseType: 'blob',
    })
    .then(res => {
      const dest = fs.createWriteStream('./out.pdf');
      res.body.pipe(dest);
    });
})();
