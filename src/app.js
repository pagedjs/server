process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const PORT = process.env.PORT || 3000;

const Koa = require('koa');
const Router = require('koa-router');

const cors = require('kcors');
const helmet = require('koa-helmet');
const bodyParser = require('koa-bodyparser');
// const koaBody = require('koa-body');
const requestLogger = require('koa-logger');
const validate = require('koa-validate');
const mount = require('koa-mount');
const serve = require('koa-static');


// const pdf = require('./pdf');
// const Printer = require('pagedjs-cli');

const printerRoute = require('./printer');

const app = new Koa();
const router = new Router();

router.post('/', printerRoute);

app.use(cors());

app.use(helmet());
app.use(requestLogger());
app.use(bodyParser(
  {
    enableTypes: ['json', 'form', 'text', 'text/plain', "text/html", "application/xhtml+xml"],
    jsonLimit: '5GB',
    textLimit: '5GB',
    strict: false
  },
));

validate(app);

app.use(router.routes())

app.use(serve('static/'));

let server = app.listen(PORT, () => {
  console.warn(`Printer server listening on ${PORT}, in ${process.env.NODE_ENV} mode`);
});

process.on('SIGTERM', () => {
  console.log('Closing http server.');
  printer.close();
  server.close();
});


module.exports = app;
