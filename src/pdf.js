const fs = require('fs');
const puppeteer = require('puppeteer');

module.exports = async (ctx) => {
  ctx.type = 'application/pdf';
  ctx.attachment('out.pdf');

  const { url, html, options, headers } = ctx.request.body;

  console.debug('URL: ', url);
  console.debug('HTML: ', html);
  console.debug('OPTIONS: ', options);
  console.debug('HEADERS: ', headers);

  const browser = await puppeteer.launch({
    ignoreHTTPSErrors: true,
    args: [
      '--no-sandbox',
      '--disable-setuid-sandbox',
      '--disable-dev-shm-usage', // https://github.com/GoogleChrome/puppeteer/blob/master/docs/troubleshooting.md#tips
    ],
  });

  try {
    const page = await browser.newPage();

    if (headers && Object.keys(headers).length) {
      await page.setExtraHTTPHeaders(headers);
    }

    await page.goto(url || `data:text/html,${html}`);

    await page.addScriptTag({
      path: __dirname + "/../../../node_modules/pagedjs/dist/paged.polyfill.js"
    });

    let width, height, orientation;
    let resolveReady;
    let ready = new Promise(function(resolve, reject) {
      resolveReady = resolve;
    });
    await page.exposeFunction('onPagesRendered', async (msg, pWidth, pHeight, pOrientation) => {
      console.log(msg, pWidth, pHeight, pOrientation);
      width = pWidth;
      height = pHeight;
      orientation = pOrientation;
      resolveReady();
    })

    await ready;

    ctx.body = await page.pdf(
      Object.assign({
        printBackground: true,
        displayHeaderFooter: false,
        width: width,
        height: height,
        orientation: orientation,
        margin: {
          top: 0,
          right: 0,
          bottom: 0,
          left: 0,
        }
      }, options),
    );
    await page.close();
  } catch (e) {
    await closeBrowser(browser);
    throw e;
  }
};
