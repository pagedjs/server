const fs = require('fs');
const Printer = require('pagedjs-cli');

const printer = new Printer();
printer.setup();

printer.on("page", (page) => {
  console.debug("Rendering: Page " + (page.position + 1))
});

printer.on("rendered", (msg) => {
  console.debug("Generating");
});

printer.on("postprocessing", (msg) => {
  console.debug("Processing");
});

module.exports = async (ctx) => {
  ctx.type = 'application/pdf';
  ctx.attachment('out.pdf');

  const { url, html, options, headers } = ctx.request.body;

  console.debug('URL: ', url);
  console.debug('HTML: ', html && html.length);
  // console.debug('OPTIONS: ', options);
  // console.debug('HEADERS: ', headers);

  let file;

  if (url) {
    file = await printer.pdf(url, options);
  } else if (html) {
    file = await printer.pdf({ html }, options);
  } else {
    file = await printer.pdf({ html: ctx.request.body});
  }

  ctx.body = Buffer.from(file);
};
