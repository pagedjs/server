class Uploader {
  constructor(onUpload) {
    this.onUpload = onUpload;

    this.form = document.getElementById("form");

    this.form.addEventListener("dragenter", this.dragenter.bind(this), false);
    this.form.addEventListener("dragover", this.dragover.bind(this), false);
    this.form.addEventListener("dragleave", this.dragleave.bind(this), false);
    this.form.addEventListener("drop", this.drop.bind(this), false);

    this.inputListener();
  }

  dragenter(e) {
    e.stopPropagation();
    e.preventDefault();
    this.form.classList.add("is-dragover");
  }

  dragover(e) {
    e.stopPropagation();
    e.preventDefault();
  }

  dragleave(e) {
    e.stopPropagation();
    e.preventDefault();
    this.form.classList.remove("is-dragover");
  }

  drop(e) {
    e.stopPropagation();
    e.preventDefault();

    var dt = e.dataTransfer;
    var files = dt.files;
    this.upload(files[0]);
  }

  inputListener() {
    let inputElement = document.getElementById("input");
    let choose = document.getElementById("choose");
    choose.addEventListener("click", () => {
      inputElement.click();
    }, false);


    inputElement.addEventListener('change', (e) => {
      var file = e.target.files[0];
      this.upload(file);
      this.form.style.display = "none";
    });
  }

  upload(file) {
    if (window.FileReader) {
      var reader = new FileReader();
      reader.onload = this.result.bind(this);
      reader.readAsArrayBuffer(file);
    }
  }

  result(e) {
    this.onUpload(e.target.result);
  }

  destroy() {

  }
}

function getPdf(html) {
  return fetch(window.location.origin, {
      method: 'POST',
      body: JSON.stringify({
        html
      }),
      headers: {
        'Content-Type': 'application/json',
      },
    })
    .then(r => r.blob())
}

let ready = new Promise(function(resolve, reject){
  if (document.readyState === "interactive" || document.readyState === "complete") {
    resolve(document.readyState);
    return;
  }

  document.onreadystatechange = function ($) {
    if (document.readyState === "interactive") {
      resolve(document.readyState);
    }
  }
});

ready.then(function () {
  let uploader = new Uploader(async (data) => {
    var dataView = new DataView(data);
    var decoder = new TextDecoder("utf-8");
    var decodedString = decoder.decode(dataView);

    let controls = document.querySelector("#controls");
    controls.style.display = "none";

    let viewer = document.querySelector("#viewer");
    viewer.style.display = "block";

    let pdf = await getPdf(decodedString);
    download(pdf, "output.pdf", "application/pdf");

    let spinner = document.querySelector(".lds-spinner");
    spinner.style.display = "none";

    let iframe = document.querySelector("#pdf");
    var file = window.URL.createObjectURL(pdf);
    iframe.src = file;

    let closer = document.querySelector("#close");
    closer.style.display = "block";
    closer.onclick = () => {
      controls.style.display = "block";
      viewer.style.display = "none";
      spinner.style.display = "inline-block";
      closer.style.display = "none";
      iframe.src = "";
    }
  });
});
